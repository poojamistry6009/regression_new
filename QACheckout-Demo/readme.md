# Node.js QACheckout Test Script
In 6.1.0 we started the Node.js/JavaScript QACheckout project. In order to run this code you will need to install Node.js on your system.
## Steps to run
1. Install Node.js.
2. Download the git project.
3. Verify Mocha.js is a module and has a version by typing mocha --version in a console.
4. To run the test cd bin and type mocha qacheckout.js

## What does the test do?

- There is a variables.js file which may need to be editted:
  * environment = 'test', 'stage', 'prod', 'wtaza';
  * exports.username = logged in user (e.g. echreist@hc1.com)
  * exports.password = user's password to login
  * exports.tenantId = this is the name of the tenant to be tested e.g. https://qacheckout.hc1.com (this is = qacheckout)
  * exports.campaignName = this is the name of the campaign name setup (usually 'QACheckout - Campaign')
  * exports.orgName = I made this a unique name of today's date, time and seconds in order to verify in the UI after the test.
  * exports.contactEmail = The contact's email address in order to send the campaign email (e.g. echreist@gmail.com)
- When the test runs it does the following:
  * 1 - Login - sets the sessionId
  * 2 - Session Init - retrieves the user information.
  * 3 - Retrieve interfaces list and gets the id for 'hc1Test' interface 
  * 4 - Retrieve viewId for Notification custom list
  * 5 - Retrieve notification customlist in collaboration center
  * 6 - Retrieve viewId for AccessControlTree custom list
  * 7 - Retrieve AccessControlTree for UACTree Id and UACTree name for sales territory
  * 8 - Retrieve UACTree for Node Id and Node Name 
  * 9 - Retrieve Specialty Id and Specialty Name of "lab" for organization save so it will trigger the rule to create Case, Task and Memo
  * 10 - Save Organization and set Specialty to "lab" to trigger the rule to create Case, Task and Memo
  * 11 - Retrieve Organization to verify it was created and get OrgId
  * 12 - Retrieve Organization Activities viewId for custom list Org Activities after the rule has created the Case, Task and Memo
  * 13 - Retrieve Organization Activities custom list and get the case, task, and memo id and name (repectively)
  * 14 - Retrieve Case and verifies the case subject is set correctly
  * 15 - Retrieve Task and verifies the task subject is set correctly
  * 16 - Retrieve Task and verifies the memo subject is set correctly
  * 17 - Retrieve Campaign viewId for Campaign custom list.
  * 18 - Retrieve Campaign custom list and get id for variables.js exports.campaignName
  * 19 - Save a contact with variables.js exports.contactEmail 
  * 20 - Assigns contact to campaign and rule triggers to send the campaign email to the contact's email.
  * 21 - Save same Organization again to verify the rule doesn't run a second time (adds description set in variables.js exports.orgDescription)
  * 22 - Does a cloud search for the Organization and verifies it is in cloud search.
  * 23 - Logs user out.
- Current Issues:
  * Need to handle bad user credentials during login currently it continues the test and errors for all steps.
  * cloud search doesn't always return results, I need to keep trying a number of times and wait for cloud search to be updated.